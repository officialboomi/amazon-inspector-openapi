# Amazon Inspector Connector
Amazon Inspector enables you to analyze the behavior of your AWS resources and to identify potential security issues.

Documentation: https://docs.aws.amazon.com/inspector/v1/APIReference/Welcome.html

Specification: https://github.com/APIs-guru/openapi-directory/blob/main/APIs/amazonaws.com/inspector/2016-02-16/openapi.yaml

## Prerequisites

+ Generate an AWS Acess Key ID and AWS Secret Key from the AWS Admin Console

## Supported Operations
All endpoints are operational.
## Connector Feedback

Feedback can be provided directly to Product Management in our [Product Feedback Forum](https://community.boomi.com/s/ideas) in the boomiverse.  When submitting an idea, please provide the full connector name in the title and a detailed description.

